export const environment = {
  production: false,
  awsRegion: 'us-east-1',
  awsCognitoIdentityPoolId: 'us-east-1:12345678-1234-1234-1234-1234567890',
  awsCognitoUserPoolId: 'us-east-1_abcd1234',
  awsCognitoClientId: 'abcdefghi1234567890',
};
