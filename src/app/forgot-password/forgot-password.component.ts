import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import {FormsModule,ReactiveFormsModule} from '@angular/forms';
//import { Auth } from 'aws-amplify';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent {
  forgotPasswordForm: FormGroup;
  forgotPasswordError: string = '';
  isCodeSent: boolean = false;
  newPasswordForm: FormGroup;
  newPasswordError: string = '';
  isPasswordReset: boolean = false;

  constructor(private formBuilder: FormBuilder) {
    this.forgotPasswordForm = this.formBuilder.group({
      username: ['', Validators.required]
    });

    this.newPasswordForm = this.formBuilder.group({
      verificationCode: ['', Validators.required],
      newPassword: ['', Validators.required]
    });
  }

  async sendVerificationCode() {
    /*try {
      const { username } = this.forgotPasswordForm.value;
      await Auth.forgotPassword(username);
      this.isCodeSent = true;
    } catch (error) {
      this.forgotPasswordError = "error:sendVerificationCode";
    }*/
  }

  async resetPassword() {
   /* try {
      const { username, verificationCode, newPassword } = this.newPasswordForm.value;
      await Auth.forgotPasswordSubmit(username, verificationCode, newPassword);
      this.isPasswordReset = true;
    } catch (error) {
      this.newPasswordError = "error:resetPassword";
    }*/
  }
}
