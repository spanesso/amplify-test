import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
//import { Auth } from 'aws-amplify';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  loginForm: FormGroup;
  loginError: string;

  constructor(private formBuilder: FormBuilder) {
    this.loginError = '';
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  async login() {
   /* try {
      const { username, password } = this.loginForm.value;
      await Auth.signIn(username, password);
      // Login successful, handle the authenticated state
    } catch (error) {
      this.loginError = "error.login";
    }*/
  }
}
